document.addEventListener('DOMContentLoaded', function() {
    const colorBar = document.getElementsByClassName('percentage');

    for (let i = 0; i < colorBar.length; i += 1) {
        let width = colorBar[i].dataset.percentage;
        colorBar[i].style.width = `${width}%`;
        colorBar[i].style.transition = 'width .8s ease-out';
    };
});

