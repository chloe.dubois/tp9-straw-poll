# TP Straw poll

## Objectifs du projet

L'objectif est de réaliser un site de sondages.

Un utilisateur doit pouvoir :
- se connecter de manière sécurisée,
- créer un sondage, 
- créer un brouillon de sondage et l'éditer,
- consulter la liste des sondages créés,
- voter dans un sondage (1 seule fois par utilisateur) et consulter les résultats.

Un sondage est constitué d’une question et de plusieurs réponses. Ces réponses peuvent être à choix unique ou multiple, et il peut y avoir une infinité de réponses.

Le site doit fournir :
- une page qui permette de voir le sondage et de répondre à la question,
- une page qui permette de voir les résultats du sondage,
- une page qui permette de créer un nouveau sondage, avec la question, les réponses possible et le type (unique ou multiple). 

Toutes les informations sont stockées dans une base de données.

## Techno employées

### Back-end

Le site est réalisé avec Symfony, et Maria DB pour la base de donnée.

### Front-end

Les technos employées sont les suivantes :
- jQuery et javascript,
- Sass




