<?php

namespace App\Form;

use App\Entity\Answer;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('answers', ChoiceType::class, [
            'choices' => $options['answers'],
            'choice_label' => 'answer',
            'expanded'=>true,
            'multiple'=> $options['multiple'],
            'mapped' => false,
            'label'=> 'Choose your answer:'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'multiple' => true,
            'answers' => array(),
            'data_class' => Answer::class, // formulaire agit sur un objet par défault
        ]);
    }
}
